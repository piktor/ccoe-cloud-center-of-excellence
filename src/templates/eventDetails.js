import React, { useEffect, useState } from "react";
import { graphql } from "gatsby";
import moment from "moment";
// eslint-disable-next-line
import { saveAs } from "file-saver";
import { Carousel } from "react-responsive-carousel";
import Layout from "../components/layout";
import Event from "../components/Section/Slider/Event/Event";
import css from "./eventDetails.module.css";
import { ics } from "./ics";
import "./eventDetailsCss.css";
import "../../node_modules/react-responsive-carousel/lib/styles/carousel.min.css";
import dummyCoverImage from "../images/img_calendar_placeholder.png";

let firstThreeEvents = [];

const EventDetails = ({ data }) => {
    const [detailPage, setDetailPage] = useState(false);
    const post = data.markdownRemark.frontmatter;
    const startDate = moment(post.startDate);
    const endDate = moment(post.endDate);
   

    let startDateInUtc = moment(post.startDate)
        .utc()
        .format("h:mm a");
    let endDateInUtc = moment(post.endDate)
        .utc()
        .format("h:mm a");

    useEffect(() => {
        if (data.upComingEvents.edges.length > 3) {
            firstThreeEvents = data.upComingEvents.edges.filter((val, i) => {
                return i < 3;
            });
        } else {
            firstThreeEvents = data.upComingEvents.edges;
        }
        setDetailPage(true);
    }, []);

    const saveToMycalender = () => {
        var cal_single = ics();
        cal_single.addEvent(
            `${post.name}`,
            `${post.description}`,
            `${post.location}`,
            `${startDate.format("l")} ${startDateInUtc}`,
            `${endDate.format("l")} ${endDateInUtc}`
        );

        cal_single.download(`${post.name}`);
    };

    return (
        <Layout eventListingPage>
            <main className={css.eventDetailContainer}>
                <section className={css.LeftEventDescription}>
                    <div className={css.eventName}>{post.name}</div>
                    <div className={css.eventDetail}>
                        <div className={css.eventLeftDetail}>
                            <div className={css.locationWrapper}>
                                <img
                                    className={css.Image}
                                    src={require("../images/modal/icon-map-pointer.svg")}
                                    alt="map"
                                />
                                <div
                                    className={css.eventLocation}
                                    style={{
                                        opacity: post.location ? "0.9" : "0.6",
                                    }}
                                >
                                    {post.location
                                        ? post.location
                                        : "No address...!"}
                                </div>
                            </div>
                            <div className={css.locationWrapper}>
                                <img
                                    className={css.Image}
                                    src={require("../images/modal/icon-calendar.svg")}
                                    alt="calendar"
                                />
                                <div className={css.eventTime}>
                                    {`${startDate.format(
                                        "Do"
                                    )} ${startDate.format(
                                        "MMM"
                                    )} - ${startDate.format("LT")}`}{" "}
                                    to{" "}
                                    {`${endDate.format("Do")}  ${endDate.format(
                                        "MMM"
                                    )} - ${endDate.format("LT")}`}
                                </div>
                            </div>
                        </div>
                        <button
                            onClick={() => saveToMycalender()}
                            className={css.saveCalenderBtn}
                        >
                            Save to my calender
                        </button>
                    </div>

                    <div className={css.eventAvatarWrapContainer}>
                        <img
                            className={css.eventAvatarWrap}
                            style={{
                                objectFit:
                                    post.coverImage == null ? "cover" : "",
                            }}
                            src={
                                post.coverImage == null
                                    ? dummyCoverImage
                                    : require(`../images/${post.coverImage}`)
                            }
                            alt="Event info"
                        />
                    </div>
                    <div className={css.eventDescription}>
                        <div className={css.descriptionHeader}>
                            {" "}
                            {post.descriptionTitle}
                        </div>
                        <p>{post.description}</p>
                    </div>
                </section>
                <section className={css.rightEventMap}>
                    <div className={css.moreEvent}>Upcoming Events</div>
                    <div className={css.eventCarousel}>
                        <Carousel
                            showThumbs={false}
                            infiniteLoop={true}
                            autoPlay={true}
                        >
                            {firstThreeEvents.map((item, key) => (
                                <div
                                    className={css.eventCardWrap}
                                    key={item.node.id}
                                >
                                    <Event isDetail={detailPage} data={item} />
                                </div>
                            ))}
                        </Carousel>
                    </div>
                </section>
            </main>
        </Layout>
    );
};

export const pagelistQuery = graphql`
    query($path: String!) {
        markdownRemark(fields: { slug: { eq: $path } }) {
            fields {
                slug
            }

            frontmatter {
                path
                name
                descriptionTitle
                description
                coverImage
                startDate
                endDate
                location
                featuredEvent
                event
            }
        }
        upComingEvents: allMarkdownRemark(
            filter: {
                frontmatter: {
                    event: { eq: true }
                    featuredEvent: { eq: false }
                }
            }
        ) {
            edges {
                node {
                    id
                    fields {
                        slug
                    }
                    frontmatter {
                        path
                        name
                        descriptionTitle
                        description
                        coverImage
                        startDate
                        endDate
                        location
                        featuredEvent
                    }
                    excerpt
                }
            }
        }
    }
`;

export default EventDetails;
