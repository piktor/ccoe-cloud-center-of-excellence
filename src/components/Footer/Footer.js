import React from "react";
import icon from "../../images/tmo-black.svg";
import css from "./Footer.module.css";

const Footer = () => (
    <div className={css.container}>
        <div className={css.tmobile}>
            <img src={icon} className={css.tmobileIcon} alt="t-mobile" />
        </div>
        <div className={css.copy}>Copyright 2019. All rights reserved.</div>
    </div>
);

export default Footer;
