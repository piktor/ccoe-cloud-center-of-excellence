/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react";
import PropTypes from "prop-types";
import { StaticQuery, graphql } from "gatsby";

import Header from "./Header/Header";
import Footer from "./Footer/Footer";
import css from "./layout.css";

const Layout = ({ children, eventListingPage }) => (
    <StaticQuery
        query={graphql`
            query SiteTitleQuery {
                site {
                    siteMetadata {
                        title
                    }
                }
            }
        `}
        render={data => (
            <div>
                <Header
                    eventListingPage={eventListingPage}
                    siteTitle={data.site.siteMetadata.title}
                />
                <div>
                    <main id="mainContainer" className={css.mainContainer}>{children}</main>
                </div>
                {eventListingPage ? null : <Footer />}
            </div>
        )}
    />
);

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
