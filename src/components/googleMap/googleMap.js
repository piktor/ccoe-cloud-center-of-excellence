import React,{Component} from "react";
import * as googleApiConfig from '../../../apiGoogleconfig.json';
import css from "./googleMap.module.css";


const loadJS = src => {
  var ref = window.document.getElementsByTagName("script")[0]
  var script = window.document.createElement("script")
  script.src = src
  script.async = true
  ref.parentNode.insertBefore(script, ref)
}

const createMarker = (place, map) => {
  let infowindow = new window.google.maps.InfoWindow()
  let marker = new window.google.maps.Marker({
    map: map,
    position: place.geometry.location,
    center: place.geometry.location,
  })

  window.google.maps.event.addListener(marker, "click", function() {
    infowindow.setContent("Event location name" + place.name)
    infowindow.open(map, this)
  })
}

class GoogleMap extends Component {
  constructor(props) {
    super(props)
    this.mapRef = React.createRef()
    this.state = {
       
    }
    this.state.address = props.address;
  }

  initMap = () => {
    var self = this
    this.map = new window.google.maps.Map(this.mapRef.current, {
      zoom: 4,
    })

    var request = {
      query: this.state.address,
      fields: ["name", "geometry"],
    }

    var service = new window.google.maps.places.PlacesService(this.map)
    service.findPlaceFromQuery(request, function(results, status) {
      if (status === window.google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          createMarker(results[i], self.map)
        }
        self.map.setCenter(results[0].geometry.location)
      }
    })
  }

  componentDidMount() {
    // Connect the initMap() function within this class to the global window context,
    // so Google Maps can invoke it
    window.initMap = this.initMap.bind(this)
    // Asynchronously load the Google Maps script, passing in the callback reference
    loadJS(
      `https://maps.googleapis.com/maps/api/js?key=${googleApiConfig.apiKey}&libraries=places&callback=initMap`
    )
  }

  render() {
    return <div ref={this.mapRef} className={css.map} />
  }
}

export default GoogleMap;