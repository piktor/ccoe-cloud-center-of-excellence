import React from "react";
import css from "./Platform.module.css";

const Platform = props => (
    <div className={css.blockWrap}>
        <a href={props.data.link} target="_blank" rel="noopener noreferrer">
            <div className={css.elements}>
                <div className={css.img}>
                    <img
                        src={require("../../../../images/platforms/" +
                            props.data.image)}
                        className={css.platformImage}
                        alt="icon"
                    />
                </div>

                <div className={css.details}>
                    <div className={css.head}>{props.data.title} </div>
                    <div className={css.desc}>{props.data.description} </div>
                </div>
            </div>
        </a>
    </div>
);

export default Platform;
