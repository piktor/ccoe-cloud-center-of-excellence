import React from "react";
import css from "./Platforms.module.css";
import Platform from "./Platform/Platform";

const sortData = data => {
    let arrayList = [];
    data.map(element => 
        arrayList[element.node.frontmatter.order] = element
    );

    return arrayList;
};

const renderPlatforms = data => {
    let sortedData = sortData(data.edges);
    return sortedData.map((item, key) => {
        return <Platform data={item.node.frontmatter} key={key} />;
    });
};

const Platforms = ({ data }) => (
    <div id="platforms-wrapper" className={css.container}>
        <div className={css.heading}>PLATFORMS</div>
        <div id="p" className={css.platform}>
            {renderPlatforms(data)}
        </div>
    </div>
);

export default Platforms;
