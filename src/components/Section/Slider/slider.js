import React from "react";
import { Link } from "gatsby";
import Slider from "react-slick";
import Event from "./Event/Event";
import css from "./slider.module.css";
import "./slider.css";

const LeftArrow = props => {
    const { className, style, onClick } = props;

    return (
        <div className={className} onClick={onClick} style={{ ...style }}>
            <img
                src={require("../../../images/arrow-left.png")}
                alt="right"
                style={{ width: `0.8em`, opacity: `0.7`, height: "30px" }}
            />
        </div>
    );
};

const RightArrow = props => {
    const { className, style, onClick } = props;

    return (
        <div className={className} onClick={onClick} style={{ ...style }}>
            <img
                src={require("../../../images/arrow-right.png")}
                alt="right"
                style={{ width: `0.8em`, opacity: `0.7`, height: "30px" }}
            />
        </div>
    );
};

var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 6000,
    customPaging: () => <div className="carousel-dots" />,
    nextArrow: <RightArrow />,
    prevArrow: <LeftArrow />,
};

class MySlider extends React.Component {
    state = {
        data: [],
        upcomingEvents: null,
        showLoader: true,
        errorMsg:
            "Uh-oh!...Error occured while fetching upcoming calender events.",
        ie: false,
    };

    renderUpcomingEvents = () => {
        if (this.props.data.edges.length) {
            // eslint-disable-next-line
            this.state.upcomingEvents = this.props.data.edges;
            return this.state.upcomingEvents.map((item, key) => {
                return (
                    <Event
                        data={item}
                        key={key}
                        getData={value => {
                            this.setState({
                                data: value.data,
                            });
                        }}
                    />
                );
            });
        } else {
            return (
                <div className={css.calenderError}>
                    <div className={css.noEvents}>
                        <div className={css.noImageholder}>
                            <img
                                className={css.noImage}
                                src={require("../../../images/no-events.svg")}
                                alt="no events"
                            />
                        </div>
                        <div className={css.textHeader}>Oops!</div>
                        <div className={css.subHeader}>
                            We don’t have any upcoming events at this moment.
                        </div>
                    </div>
                </div>
            );
        }
    };

    onLeftArrowClick = (event, prev) => {
        if (typeof document !== "undefined") {
            let backgroundPos = document.querySelector("#caraousal-cont").style
                .backgroundPositionX;
            let scrollWidth;

            if (backgroundPos === "" || parseInt(backgroundPos) <= 0) {
                scrollWidth = 0;
            } else {
                scrollWidth = parseInt(backgroundPos) - 10;
            }

            if (backgroundPos !== "" || backgroundPos !== "0%") {
                document.querySelector(
                    "#caraousal-cont"
                ).style.backgroundPositionX = scrollWidth + "%";
            }
            prev();
        }
    };

    render() {
        return (
            <Slider {...settings}>
                <div className={css.cloud}>
                    <div className={css.head}>
                        <div className={css.headingLine} />
                        Vision Statement
                        <div className={css.headingLine} />
                    </div>
                    <div className={css.bodyPrime}>
                        Drive T-Mobile’s digital future and move at Un-carrier
                        speed by focusing on unleashing the full potential of
                        our people and building an automated operating model
                        that frees teams to innovate.
                    </div>
                    <div className={css.imageHolder}>
                        {this.state.ie === true ? (
                            <img
                                src={require("../../../images/ballon.png")}
                                alt="tuesday"
                                className={css.tuesday}
                            />
                        ) : (
                            <img
                                src={require("../../../images/tuesday.svg")}
                                alt="tuesday"
                                className={css.tuesday}
                            />
                        )}
                        <img
                            src={require("../../../images/rebillion.png")}
                            alt="rebellion"
                            className={css.rebelion}
                        />
                        <img
                            src={require("../../../images/img-mp-cs.svg")}
                            alt="mp"
                            className={css.mp}
                        />
                    </div>
                </div>
                <div className={css.cloud}>
                    <div className={css.head}>
                        <div className={css.headingLine} />
                        Our Offerings <div className={css.headingLine} />
                    </div>
                    <div className={css.body}>
                        Empower your ideas and design solutions in cloud
                        services that are changing the future.{" "}
                    </div>
                    <div className={css.imageHolder}>
                        <div className={css.aws}>
                            <img
                                src={require("../../../images/icon-aws.png")}
                                className={css.awsImage}
                                alt="aws"
                            />
                            <div className={css.textHead}>
                                Amazon Web Services
                            </div>
                            <div className={css.text}>
                                Providing on-demand cloud computing platforms to
                                individuals, companies and governments.
                            </div>
                        </div>
                        <div className={css.line} />
                        <div className={css.azure}>
                            <img
                                src={require("../../../images/icon-azure.png")}
                                className={css.azureImage}
                                alt="azure"
                            />
                            <div className={css.textHead}>Microsoft Azure</div>
                            <div className={css.text}>
                                Cloud computing service created for building,
                                testing, deploying, and managing applications
                                and services.
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css.cardBlock}>
                    <div className={css.head}>
                        <div className={css.headingLine} />
                        Upcoming Events
                        <div className={css.headingLine} />
                    </div>
                    <div className={css.body}>
                        Keep up to date on new releases and Upcoming Events
                    </div>
                    <div className={css.event}>
                        {this.renderUpcomingEvents()}
                    </div>
                    {this.state.upcomingEvents &&
                    this.state.upcomingEvents.length > 3 ? (
                        <div className={css.allEventsButton}>
                            <Link to="/eventList">VIEW ALL EVENTS</Link>
                        </div>
                    ) : null}
                </div>
            </Slider>
        );
    }
}

export default MySlider;
