import React from "react";
import css from "./EventModal.module.css";
import Dialog from "@material-ui/core/Dialog";
import Slide from "@material-ui/core/Slide";

function Transition(props) {
    return <Slide direction="up" {...props} />;
}
class EventModal extends React.Component {
    state = {
        isFull: false,
    };
    close = () => {
        this.props.closeModal(false);
    };

    getEventTime = (start, end) => {
        let endDate = new Date(end);
        let startDate = new Date(start);

        endDate = endDate.toLocaleString("en-US", {
            hour: "numeric",
            hour12: true,
        });
        startDate = startDate.toLocaleString("en-US", {
            hour: "numeric",
            hour12: true,
        });
        return startDate + " - " + endDate;
    };

    getEventDate = startDate => {
        let eventDate = new Date(startDate);
        let months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        let date = eventDate.getDate();
        let eventMonth = months[eventDate.getMonth()];
        let year = eventDate.getFullYear();

        return date + " " + eventMonth + " " + year;
    };

    componentWillMount = () => {
        if (typeof window !== `undefined` && window.innerWidth < 600) {
            this.setState({ isFull: true });
        } else {
            this.setState({
                isFull: false,
            });
        }
    };
    render() {
        return (
            <Dialog
                open={true}
                fullScreen={this.state.isFull}
                TransitionComponent={Transition}
            >
                <div className={css.modalView}>
                    <div className={css.Details}>{this.props.item.summary}</div>
                    <div className={css.cross} onClick={() => this.close()}>
                        <img
                            className={css.icon}
                            src={require("../../../../../images/modal/icon-close.svg")}
                            alt="close"
                        />
                    </div>
                    <div className={css.clockWrap}>
                        <div>
                            <img
                                className={css.Image}
                                src={require("../../../../../images/modal/icon-clock.svg")}
                                alt="clock"
                            />
                        </div>
                        <div className={css.dateTime}>
                            <div className={css.Date}>
                                {this.props.item.start.dateTime
                                    ? this.getEventDate(
                                          this.props.item.start.dateTime
                                      )
                                    : this.getEventDate(
                                          this.props.item.start.date
                                      )}
                            </div>
                            <div className={css.Time}>
                                {this.props.item.start.dateTime
                                    ? this.getEventTime(
                                          this.props.item.start.dateTime,
                                          this.props.item.end.dateTime
                                      )
                                    : ""}
                            </div>
                        </div>
                    </div>

                    <div className={css.addressWrap}>
                        <div>
                            <img
                                className={css.Image}
                                src={require("../../../../../images/modal/icon-map-pointer.svg")}
                                alt="map"
                            />
                        </div>
                        <div className={css.Address}>
                            {this.props.item.location || "N/A"}
                        </div>
                    </div>

                    <div className={css.aboutWrap}>
                        <div className={css.ImgWrap}>
                            <img
                                className={css.Image}
                                src={require("../../../../../images/modal/icon-code.svg")}
                                alt="code"
                            />
                        </div>
                        <div className={css.about}>
                            {" "}
                            {this.props.item.description.replace(
                                /<(?:.|\n)*?>/gm,
                                ""
                            ) || "N/A"}
                        </div>
                    </div>
                    <div className={css.url}>
                        <a
                            target="_blank"
                            href={this.props.item.htmlLink || "#"}
                        >
                            View Event
                        </a>
                    </div>
                    <div className={css.calendarWrap}>
                        <div>
                            <img
                                className={css.Image}
                                src={require("../../../../../images/modal/icon-calendar.svg")}
                                alt="calendar"
                            />
                        </div>
                        <div className={css.created}>
                            {" "}
                            {this.props.item.organizer.displayName || "_"}
                        </div>
                    </div>
                </div>
            </Dialog>
        );
    }
}

export default EventModal;
