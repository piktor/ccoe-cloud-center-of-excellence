import React from "react";
import css from "./Event.module.css";
import { Link } from "gatsby";
import moment from "moment";
let windowLocation = typeof window !== `undefined` && window.location;
let windowLocationHref = windowLocation.href || "";
const isEventListingPage = path => {
    if (path === "/") {
        return css.homePageEventCards;
    } else {
        if (windowLocationHref.includes("eventDetails")) {
            return css.detailContainer;
        } else return "";
    }
};
class Event extends React.Component {
    onContainerHover = (e, state) => {
        if (windowLocationHref.includes("/eventDetails")) {
            return;
        }

        let containerRef = e.currentTarget;
        if (state) {
            containerRef.style.transform = "scale(1.1)";
        } else {
            containerRef.style.transform = "scale(1)";
        }
    };

    getEventTime = (start, end) => {
        let endDate = new Date(end);
        let startDate = new Date(start);

        endDate = endDate.toLocaleString("en-US", {
            hour: "numeric",
            hour12: true,
        });
        startDate = startDate.toLocaleString("en-US", {
            hour: "numeric",
            hour12: true,
        });
        return startDate + " - " + endDate;
    };

    getEventDate = startDate => {
        let eventDate = new Date(startDate);
        let months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        let date = eventDate.getDate();
        let eventMonth = months[eventDate.getMonth()];

        return date + " " + eventMonth;
    };

    getDate(startDate, endDate) {
        const startDay = moment(startDate).format("Do");
        const endDay = moment(endDate).format("Do");
        const startMonth = moment(startDate).format("MMM");
        const endMonth = moment(endDate).format("MMM");
        if (startDay === endDay && startMonth === endMonth) {
            return (
                <div>
                    <div>{startDay}</div>
                    <div style={{fontSize:"0.85em"}}>{startMonth}</div>
                </div>
            );
        } else if (startDay !== endDay && startMonth === endMonth) {
            return (
                <div>
                    <div>
                        {startDay}-{endDay}
                    </div>
                    <div style={{fontSize:"0.85em"}}>{startMonth}</div>
                </div>
            );
        } else
            return (
                <div style={{ display: "flex" }}>
                    <div>
                        <div>{startDay}</div>
                        <div style={{fontSize:"0.85em"}}>{startMonth}</div>
                    </div>
                    <div style={{ alignSelf: "center", padding: "0 0.25em" }}>
                        -
                    </div>
                    <div>
                        <div>{endDay}</div>
                        <div style={{fontSize:"0.85em"}}>{endMonth}</div>
                    </div>
                </div>
            );
    }

    getFormatedTime(startDate, endDate) {
        const date =
            moment(startDate).format("LT") + "-" + moment(endDate).format("LT");
        return date;
    }

    render() {
        const { isDetail } = this.props;
        var data = this.props.data.node;
        windowLocation = typeof window !== `undefined` && window.location;
        windowLocationHref = windowLocation.href || "";

        return (
            <Link
                to={data.fields.slug}
                className={
                    css.container +
                    " " +
                    isEventListingPage(windowLocation.pathname)
                }
                onMouseOver={e => this.onContainerHover(e, true)}
                onMouseOut={e => this.onContainerHover(e, false)}
            >
                <div>
                    <div
                        className={
                            isDetail
                                ? css.cardTopSectionForCarousel
                                : css.cardTopSection
                        }
                        style={{
                            backgroundImage:
                                data.frontmatter.coverImage === null
                                    ? `url(${require("../../../../images/img_calendar_placeholder.png")})`
                                    : `url(${require("../../../../images/" +
                                          data.frontmatter.coverImage)})`,
                        }}
                    >
                        <div className={css.eventDate}>
                            <div className={css.eventDay}>
                                {this.getDate(
                                    data.frontmatter.startDate,
                                    data.frontmatter.endDate
                                )}
                            </div>
                        </div>
                    </div>
                    <div className={css.cardBottomSection}>
                        <div className={css.eventName}>
                            {data.frontmatter.name}
                        </div>
                        <div
                            className={css.timings}
                            style={{ marginBottom: isDetail ? 0 : "10px" }}
                        >
                            <img
                                className={
                                    isDetail
                                        ? css.detailAccesTime
                                        : css.location
                                }
                                src={require("../../../../images/icon_time.svg")}
                                alt=""
                            />
                            <div className={css.time}>
                                {this.getFormatedTime(
                                    data.frontmatter.startDate,
                                    data.frontmatter.endDate
                                )}
                            </div>
                        </div>
                        <div className={css.timings}>
                            <img
                                className={
                                    isDetail
                                        ? css.detailAccesTime
                                        : css.location
                                }
                                src={require("../../../../images/icon_map_pointer.svg")}
                                alt=""
                            />
                            <div
                                className={css.time}
                                style={{
                                    opacity: data.frontmatter.location
                                        ? "1"
                                        : "0.6",
                                }}
                            >
                                {data.frontmatter.location
                                    ? data.frontmatter.location
                                    : "No address...!"}
                            </div>
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default Event;
