import React from "react";
import css from "./feedback.module.css";
import Dialog from "@material-ui/core/Dialog";
import StarRatings from "react-star-ratings";
import axios from "axios";
import Loader from "../Loader/loader";

const requestURL =
    "https://cloud-api.corporate.t-mobile.com/api/ccoe/v1/feedback";

class Feedback extends React.Component {
    constructor(props) {
        super(props);
        this.yesRef = React.createRef();
        this.noRef = React.createRef();
        this.state = {
            isFull: false,
            success: false,
            modalState: 1,
            rating: 0,
            dofind: true,
            feedback: "",
        };
        this.submit = this.submit.bind(this);
    }

    close = () => {
        this.props.closeModal(false);
    };
    changeRating(newRating, name) {
        this.setState({
            rating: newRating,
        });
    }
    userOption() {
        this.setState({ dofind: true });
        this.yesRef.current.style.background = "#ccc";
        this.noRef.current.style.background = "#fff";
    }
    noOption() {
        this.setState({ dofind: false });
        this.noRef.current.style.background = "#ccc";
        this.yesRef.current.style.background = "#fff";
    }

    submit(e) {
        e.preventDefault();

        let mydata =
            e.target[0].value +
            " , rating :" +
            this.state.rating.toString() +
            "/5";
        var self = this;
        this.setState({
            modalState: 0,
        });
        axios({
            method: "post",
            url: requestURL,
            data: { description: mydata },
            config: {
                headers: {
                    accept: "application/json",
                    "Content-Type": "application/json",
                },
            },
        })
            .then(function(response) {
                self.setState({
                    success: true,
                    modalState: 2,
                });
            })
            .catch(function(response) {
                self.setState({
                    success: false,
                    modalState: -1,
                });
            });
    }
    showThanks = () => {
        if (this.state.modalState === 2) {
            return (
                <div className={css.modalView}>
                    <img
                        className={css.thanksImg}
                        src={require("../../../images/success.svg")}
                        alt="success"
                    />
                    <button className={css.thanksClose} onClick={this.close}>
                        CLOSE
                    </button>
                    {this.hideModal()}
                </div>
            );
        } else if (this.state.modalState === 0) {
            return (
                <div className={css.loaderView}>
                    <Loader />
                </div>
            );
        } else if (this.state.modalState === -1) {
            return (
                <div className={css.modalView}>
                    <img
                        className={css.thanksImg}
                        src={require("../../../images/errormsg.svg")}
                        alt="error"
                    />
                    <button className={css.thanksClose} onClick={this.close}>
                        CLOSE
                    </button>
                    {this.hideModal()}
                </div>
            );
        }
    };
    hideModal = () => {
        setTimeout(() => {
            this.close();
        }, 400000);
    };

    render() {
        return (
            <Dialog open={true} fullScreen={this.state.isFull}>
                {this.state.modalState === 1 ? (
                    <div className={css.modalView}>
                        <form onSubmit={this.submit}>
                            <div
                                className={css.cross}
                                onClick={() => this.close()}
                            >
                                <img
                                    className={css.icon}
                                    src={require("../../../images/modal/icon-close.svg")}
                                    alt="close"
                                />
                            </div>

                            <div className={css.Details}>Share Feedback</div>
                            <div className={css.clockWrap}>
                                <div>
                                    Did you find what you were looking for ?
                                </div>
                                <div className={css.btnWrap}>
                                    <div
                                        ref={this.yesRef}
                                        className={css.optionf}
                                        onClick={this.userOption.bind(this)}
                                    >
                                        Yes
                                    </div>
                                    <div
                                        ref={this.noRef}
                                        className={css.option}
                                        onClick={this.noOption.bind(this)}
                                    >
                                        No
                                    </div>
                                </div>
                            </div>
                            <div className={css.exp}>
                                <div className={css.rate}>
                                    How would you rate your experience?
                                </div>
                                <div className={css.stars}>
                                    <StarRatings
                                        rating={this.state.rating}
                                        starRatedColor="#e20074"
                                        starDimension="24px"
                                        starHoverColor="#e20074"
                                        changeRating={this.changeRating.bind(
                                            this
                                        )}
                                        numberOfStars={5}
                                        name="rating"
                                    />
                                </div>
                            </div>
                            <div className={css.improveWrap}>
                                <div className={css.text}>
                                    How can we improve your experience ?
                                </div>
                                <div>
                                    <textarea />
                                </div>
                            </div>
                            {/* <div>
                            <div  className = {css.text}>  Share your email so we can reach out to help!</div>
                            <div><input className={css.email} type = 'text'   ref={el => this.element = el}/></div>
                        </div> */}
                            <div className={css.forms}>
                                <input
                                    className={css.feedback}
                                    type="submit"
                                    value="SEND FEEDBACK"
                                />
                            </div>
                        </form>
                    </div>
                ) : (
                    this.showThanks()
                )}
            </Dialog>
        );
    }
}

export default Feedback;
