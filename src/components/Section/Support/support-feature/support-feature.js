import React from "react";
import css from "./support-feature.module.css";

const Feature = props => {
    return (
        <div className={css.blockWrap}>
            <a href={props.data.link} target="_blank" rel="noopener noreferrer">
                <div className={css.img}>
                    <img
                        src={require("../../../../images/support/" +
                            props.data.image)}
                        style={{ margin: `0`, width: `3em`, height: `3em` }}
                        alt="support"
                    />
                </div>
                <div className={css.border} />
                <div className={css.details}>
                    <div className={css.head}>{props.data.title}</div>
                    <div className={css.desc}>{props.data.description} </div>
                </div>
            </a>
        </div>
    );
};

export default Feature;
