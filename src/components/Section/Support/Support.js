import React from "react";
import css from "./Support.module.css";
import Feature from "./support-feature/support-feature";

const sortData = data => {
    let arrayList = [];
    data.map(element => (arrayList[element.node.frontmatter.order] = element));

    return arrayList;
};

const renderTabs = data => {
    let sortedData = sortData(data.edges);
    return sortedData.map((item, key) => {
        return <Feature data={item.node.frontmatter} key={key} />;
    });
};

const Support = ({ data }) => (
    <div
        id="support-wrapper"
        className={css.container + " " + css.SupportWrapper}
    >
        <div className={css.blueHeader}> HELP & SUPPORT</div>
        <div className={css.features}>{renderTabs(data)}</div>
        <div className={css.bg}>
            <img
                src={require("../../../images/img-waves.svg")}
                className={css.imgRev}
                alt="waves"
            />
        </div>
        <div className={css.bgUp}>
            <img
                src={require("../../../images/img-waves.svg")}
                className={css.img}
                alt="waves"
            />
        </div>
    </div>
);

export default Support;
