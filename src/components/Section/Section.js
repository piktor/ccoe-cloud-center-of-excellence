import React from "react";
import css from "../Section/Section.module.css";
import Support from "./Support/Support";
import Platforms from "./Platforms/Platforms";
import Values from "./Values/Values";
import Blogs from "./Blogs/Blogs";
import Team from "./Team/Team";
import MySlider from "./Slider/slider";
import cloud1 from "../../images/cloud-1.svg";
import cloud2 from "../../images/cloud-2.svg";
import cloud3 from "../../images/cloud-3.svg";
import $ from "jquery";

let dotContainerRef = null;

const SectionArray = [
    {
        name: "support-wrapper",
        tooltip: "Help & Support",
    },
    {
        name: "platforms-wrapper",
        tooltip: "Platforms",
    },
    {
        name: "values",
        tooltip: "Our Values",
    },
    {
        name: "team",
        tooltip: "Our Team",
    },
    {
        name: "location",
        tooltip: "Our Location",
    },
    {
        name: "blogs",
        tooltip: "Blogs",
    },
];

class Section extends React.Component {
    state = {
        Modal: false,
        showToolValue: null,
    };

    componentDidMount() {
        if (typeof document !== "undefined") {
            document.addEventListener("keydown", this.requestReRender);
        }
        let element = document.getElementById("scrollDot");
        let self = this;
        $(window).scroll(function() {
            let scroll = $(window).scrollTop();
            if (scroll > 550) {
                element.style.opacity = "1";
                element.style.visibility = "visible";
                element.style.zIndex = "12";
                element.style.top = "50%";
                self.updateVerticalCarouselDots(scroll);
            } else {
                element.style.opacity = "0";
                element.style.visibility = "hidden";
                element.style.zIndex = "-5";
                element.style.top = "85%";
            }
        });
    }

    updateVerticalCarouselDots = offset => {
        let index = 0;

        if (offset >= 5000) {
            index = 5;
        } else if (offset >= 4250) {
            index = 4;
        } else if (offset >= 3450) {
            index = 3;
        } else if (offset >= 2700) {
            index = 2;
        } else if (offset >= 1793) {
            index = 1;
        } else index = 0;

        if (dotContainerRef) {
            for (
                let index = 0;
                index < dotContainerRef.children.length;
                index++
            ) {
                dotContainerRef.children[index].classList.remove(css.activeDot);
            }
            dotContainerRef.children[index].classList.add(css.activeDot);
        }
    };

    requestReRender = e => {
        // generateString(e);
    };

    selectTooltip = data => {
        this.setState({ showToolValue: data });
    };

    hideTooltip = () => {
        this.setState({ showToolValue: null });
    };

    renderDots = () => {
        return SectionArray.map((item, index) => {
            return (
                <div
                    className={css.dotValue}
                    key={index}
                    id={index + "web"}
                    onClick={() => this.moveSection(item.name)}
                    onMouseOut={() => this.hideTooltip()}
                    onMouseOver={() => this.selectTooltip(index)}
                >
                    <div
                        className={
                            css.dotsTooltip +
                            (this.state.showToolValue === index
                                ? " " + css.tooltipActive
                                : "")
                        }
                    >
                        {item.tooltip}
                    </div>
                </div>
            );
        });
    };

    moveSection = idValue => {
        
        let scrollTo = "";
        scrollTo = "#" + idValue;
        $("html, body").animate(
            {
                scrollTop: $(scrollTo).offset().top,
            },
            500
        );
    };

    render() {
        return (
            <div id="sectionWrapper" className={css.wrapper}>
                <div className={css.caraousalContainer} id="caraousal-cont">
                    <div className={css.heading}>
                        Cloud Center of Excellence
                    </div>
                    <img className={css.cloud1} src={cloud1} alt="clouds"/>
                    <img className={css.cloud2} src={cloud2} alt="clouds"/>
                    <img className={css.cloud3} src={cloud3} alt="clouds"/>
                    <MySlider
                        data={this.props.events}
                        showOverlay={e => {
                            this.setState(
                                {
                                    Modal: e,
                                },
                                () => {
                                    if (this.state.Modal) {
                                        // document.querySelector("html").className = "Modal";
                                        // window.scrollTo(0, 100);
                                    } else {
                                        // document.querySelector("html").className = "close";
                                    }
                                }
                            );
                        }}
                    />
                </div>
                <div
                    ref={ref => (dotContainerRef = ref)}
                    className={css.scrollDots}
                    id="scrollDot"
                >
                    {this.renderDots()}
                </div>
                <Support data={this.props.support} />
                <Platforms data={this.props.platforms} />
                <Values />
                <Team />
                <Blogs data={this.props.blogs} />
            </div>
        );
    }
}

export default Section;
