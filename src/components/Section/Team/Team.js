import React from "react";
import css from "./Team.module.css";
import ad from "../../../images/team.jpg";
const Team = () => (
    <div className={css.container} id="team-wrapper">
        <div className={css.Team} id="team">
            <div className={css.teamHeading}>OUR TEAM</div>
            <img src={ad} alt="team" className={css.adults} />
        </div>
        <div className={css.location} id="location">
            <img src={require("../../../images/location-map.png")} alt="location map" />
            <div className={css.locationTitle}>OUR LOCATION</div>
        </div>
        <div className={css.mobileMap}>
            <img
                src={require("../../../images/map-location.png")}
                style={{ width: `100%` }}
                alt="location map"
            />
        </div>
    </div>
);

export default Team;
