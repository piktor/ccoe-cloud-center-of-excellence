import React from "react";
import css from "./blog.module.css";

const getDate = dateString => {
    let strArray = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];
    let date = new Date(dateString);
    let d = date.getDate();
    let m = strArray[date.getMonth()];
    let y = date.getFullYear();
    return "" + (d <= 9 ? "0" + d : d) + " " + m + ", " + y;
};

const getBlogUrl = blogEntry => {
    let blogPath = blogEntry.node.fileAbsolutePath;
    blogPath = blogPath.slice(blogPath.lastIndexOf("/") + 1).replace(".md", "");
    const baseUrl = "https://opensource.t-mobile.com/blog/posts/";
    return baseUrl + blogPath;
};

const Blog = ({ data }) => {
    return (
        <a className={css.container} href={getBlogUrl(data)} target="_blank" rel="noopener noreferrer">
            <div className={css.left}>
                <div className={css.title}>
                    {data.node.frontmatter.title || "Blog Title"}
                </div>
                <div className={css.description}>
                    {data.node.excerpt || "Blog description or summary"}
                </div>
            </div>
            <div className={css.right}>
                <div className={css.author}>
                    {" "}
                    By : {data.node.frontmatter.author || "Blog Author"}
                </div>
                <div className={css.date}>
                    {getDate(data.node.frontmatter.date)}{" "}
                </div>
            </div>
        </a>
    );
};

export default Blog;
