import React from "react";
import css from "./blogs.module.css";
import Blog from "./Blog/Blog";

const Blogs = ({ data }) => {
    
    return (
        <div className={css.container} id="blogs">
            <div className={css.heading}>JOIN US AT </div>
            <div className={css.uncarrierWrapper}>
                <a
                    href="https://opensource.corporate.t-mobile.com"
                    target="_blank"
                    className={css.openSource}
                    rel="noopener noreferrer"
                >
                    <div className={css.text}>
                        <div className={css.lineBar} />
                        <div className={css.open}>OPEN SOURCE </div>
                        <span className={css.at}>AT THE </span>
                    </div>
                    <div className={css.textSec}>
                        <span className={css.uncarrier}>UN-CARRIER</span>
                    </div>
                </a>
            </div>

            <div className={css.para}>
                Transforming wireless through technology innovation.
            </div>

            <div className={css.blogsWrapper}>
                {data.edges.map((item, index) =>
                    index < 3 ? <Blog key={index} data={item} /> : ""
                )}
            </div>
            <a
                href="https://opensource.corporate.t-mobile.com/blog/posts/"
                target="_blank"
                className={css.allBlogs}
                rel="noopener noreferrer"
            >
                VIEW ALL BLOG POSTS
            </a>
        </div>
    );
};

export default Blogs;
