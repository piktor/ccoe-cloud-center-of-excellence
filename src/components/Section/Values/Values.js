import React, { useEffect, useState } from "react";
import css from "./Values.module.css";

import agility_png from "../../../images/img-agility@3x.png";
import security_png from "../../../images/img-security@3x.png";
import optimization_png from "../../../images/cost-optimization@3x.png";

const Values = () => {
    useEffect(() => {
        if (typeof window !== `undefined` && window.navigator.vendor === "") {
            setIEState(true);
        }
    }, []);

    const [ieState, setIEState] = useState(false);

    return (
        <div className={css.container} id="values">
            <div className={css.heading}>OUR VALUES</div>
            <div className={css.innerWrapper}>
                <div className={css.agility}>
                    <img
                        src={
                            ieState === true
                                ? agility_png
                                : require("../../../images/img-agility.svg")
                        }
                        className={css.agilityImage}
                        alt="Agility"
                    />
                    <div className={css.valueHead}>AGILITY</div>

                    <div className={css.valueDesc}>
                        Agile and nimble like gymnasts using a combination of
                        expertise, speed and balance to drive success.
                    </div>
                    <div className={css.bar} />
                </div>
                <div className={css.security}>
                    <img
                        src={
                            ieState === true
                                ? security_png
                                : require("../../../images/img-security.svg")
                        }
                        className={css.sec}
                        alt="Security"
                    />
                    <div className={css.valueHead}>SECURITY</div>

                    <div className={css.valueDesc}>
                        Safeguarding your critical data with specialized
                        products by enabling advanced threat protection
                        techniques.
                    </div>
                    <div className={css.bar} />
                </div>
                <div className={css.optimization}>
                    <img
                        src={
                            ieState === true
                                ? optimization_png
                                : require("../../../images/cost-optimization.svg")
                        }
                        className={css.opt}
                        alt="Cost optimization"
                    />
                    <div className={css.valueHead}> COST OPTIMIZATION</div>

                    <div className={css.valueDesc}>
                        Standardizing, simplifying and rationalizing to obtain
                        the best terms for our products so we can maximize
                        business value.
                    </div>
                    <div className={css.bar} />
                </div>
            </div>

            <div className={css.bg}>
                <img
                    src={require("../../../images/img-waves.svg")}
                    className={css.wave}
                    alt="wave"
                />
            </div>
        </div>
    );
};

export default Values;
