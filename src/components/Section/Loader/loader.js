import React from "react";
import "./loader.css";

const Loader = () => {
    return (
        <section className="loading-container ">
            <div className="lds-ellipsis">
                <div />
                <div />
                <div />
                <div />
            </div>
        </section>
    );
};

export default Loader;
