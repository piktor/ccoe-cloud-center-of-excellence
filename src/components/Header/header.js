import React, { useState, useEffect } from "react";
import { Link } from "gatsby";
import $ from "jquery";
import css from "./Header.module.css";
import icon from "../../images/ccoe.png";
import darkIcon from "../../images/ccoe-dark.png";
import navList from "../../jsondata/navigation";
//import FeedBack from "../Section/Feedback/Feedback";

const Header = () => {
    const [navData, updateNavData] = useState(navList["support"]);
    const [currentTab, setCurrentTab] = useState("");
    //const [isFeedBack, setFeedback] = useState(false);
    let navBarRef = null;
    let drawRef = null;
    let headerRef = null;
    let headerLogo = null;
    let navNodeRef = null;
    let windowLocation = typeof window !== `undefined` && window.location;
    let windowLocationHref = windowLocation.href || "";

    const setNavActive = (e, element) => {
        e.currentTarget.firstElementChild.src = require("../../images/" +
            element.icon.active);
        e.currentTarget.lastElementChild.style.color = "#ec2b88";
        e.currentTarget.firstElementChild.style.transform = "scale(1.05)";
    };

    const setNavInactive = (e, element) => {
        e.currentTarget.firstElementChild.src = require("../../images/" +
            element.icon.default);
        e.currentTarget.lastElementChild.style.color = "#446383";
        e.currentTarget.firstElementChild.style.transform = "scale(1)";
    };

    useEffect(() => {
        windowLocation = typeof window !== `undefined` && window.location;
        windowLocationHref = windowLocation.href || "";
        if (typeof window !== "undefined") {
            let element = document.getElementById("heading");
            $(window).scroll(function() {
                let scroll = $(window).scrollTop();
                if (scroll > 50) {
                    element.style.background = "#466686";
                } else {
                    element.style.background = "transparent";
                }
            });
        }
    }, []);

    const onNavItemClick = element => {
        var id;
        setTimeout(function() {
            let platforms = [
                "t-vault",
                "conducktor",
                "insight",
                "jazz",
                "next",
                "pacbot",
                "telemetry",
            ];

            if (platforms.indexOf(element) < 0) {
                id = document.getElementById("support-wrapper");
            } else {
                id = document.getElementById("platforms-wrapper");
            }
            if (id.offsetHeight) {
                $("html, body").animate(
                    {
                        scrollTop: $(id).offset().top,
                    },
                    500
                );
            }
        }, 100);
    };

    const renderNav = () => {
        return navData.map((item, index) => {
            return (
                <Link
                    to="/"
                    onClick={() => onNavItemClick(item.title)}
                    onMouseEnter={event => setNavActive(event, item)}
                    onMouseLeave={event => setNavInactive(event, item)}
                    className={css.navElement}
                    key={index}
                >
                    <img
                        className={css.navElementIcon}
                        src={require("../../images/" + item.icon.default)}
                        alt="nav element"
                    />
                    <p className={css.navElementText}>{item.title}</p>
                    <div className={css.dummyElement} />
                </Link>
            );
        });
    };
    const hideNavBar = () => {
        let active = css.active,
            arrowIconNodes;
        navBarRef.style.opacity = "0";
        navBarRef.style.transform = "translateY(-100%)";
        headerRef.classList.remove(css.activeNav);
        navBarRef.classList.remove(css.active);
        headerLogo.src = icon;
        if (typeof document !== "undefined") {
            arrowIconNodes = document.querySelectorAll("." + active);
        }
        for (let index = 0; index < arrowIconNodes.length; index++) {
            arrowIconNodes[index].classList.remove(active);
        }
    };
    const showSlide = () => {
  
        document.getElementById("mainContainer").style.backgroundColor = "rgba(0,0,0,0.4)";
        document.getElementById("mainContainer").style.opacity = "0.5";

        drawRef.style.transform = "translate(0%)";
    };
    const closeSlide = () => {
        drawRef.style.transform = "translate(-100%)";
        document.getElementById("mainContainer").style.background = "";
        document.getElementById("mainContainer").style.opacity = "";
    };
    const HelpDropDown = () => {
        setTimeout(function() {
            var node = document.getElementById("support-wrapper");
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 1000);
        closeSlide();
    };

    const platformDropDown = () => {
        setTimeout(function() {
            var node = document.getElementById("platforms-wrapper");
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 1000);
        closeSlide();
    };

    const teamScrollDown = () => {
        setTimeout(function() {
            var node = document.getElementById("team-wrapper");
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 1000);
        closeSlide();
    };

    // const showFeedback = () => {
    //     setFeedback(true);
    // };

    const goToPlatformSection = () => {
        setTimeout(function() {
            var node = document.getElementById("platforms-wrapper");
            console.log(node);
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 10);
    };

    const goToHelpSection = () => {
        setTimeout(function() {
            var node = document.getElementById("support-wrapper");
            console.log(node);
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 1000);
    };

    const goToAboutUsSection = () => {
        setTimeout(function() {
            var node = document.getElementById("team-wrapper");
            if (node) {
                node.scrollIntoView({ behavior: "smooth" });
            }
        }, 1000);
    };

    const showNavbar = updateState => {
        let activenode = css.activeNav;
        navBarRef.style.opacity = "1";
        navBarRef.style.transform = "translateY(0px)";
        headerRef.classList.add(activenode);
        navBarRef.classList.add(css.active);
        headerLogo.src = darkIcon;
        if (updateState === undefined) {
            let tabs = navNodeRef;
            if (currentTab === "support") {
                tabs.children[0].classList.add(css.active);
            } else {
                tabs.children[1].classList.add(css.active);
            }
        }
    };

    const animateNavIcons = () => {
        for (let index = 0; index < navBarRef.childNodes.length; index++) {
            let navElement = navBarRef.childNodes[index];
            navBarRef.childNodes[index].style.opacity = "0";
            navElement.style.transform = "scale(0.99)";
            setTimeout(() => {
                navElement.style.opacity = "1";
                navElement.style.transform = "scale(1)";
                navElement.children[2].style.opacity = "0";
                navElement.children[2].style.zIndex = "-9";
            }, 500);
        }
    };

    const isEventListingPage = path => {
        if (
            windowLocationHref.includes("eventList") ||
            windowLocationHref.includes("eventDetails")
        ) {
            return css.eventListingHeading;
        }
    };

    const onHeaderIconHover = event => {
        let active = css.active,
            arrowIconNodes;
        updateNavData(navList[event.currentTarget.dataset.target]);
        if (typeof document !== "undefined") {
            arrowIconNodes = document.querySelectorAll("." + active);
        }
        for (let index = 0; index < arrowIconNodes.length; index++) {
            arrowIconNodes[index].classList.remove(active);
        }
        event.currentTarget.classList.add(active);
        if (event.currentTarget.dataset.target !== currentTab) {
            animateNavIcons();
        }
        setCurrentTab(event.currentTarget.dataset.target);
        showNavbar(false);
    };

    return (
        <header className={css.header} id="heading">
            <div
                ref={ref => (headerRef = ref)}
                className={
                    css.container +
                    " " +
                    isEventListingPage(windowLocation.pathname)
                }
            >
                <div className={css.burger}>
                    <img
                        src={require("../../images/burger-menu.png")}
                        style={{
                            width: `30px`,
                            margin: `0`,
                            cursor: `pointer`,
                        }}
                        onClick={() => showSlide()}
                        alt="menu"
                    />
                </div>
                <div className={css.drawer} ref={ref => (drawRef = ref)}>
                    <div className={css.drawerWrap}>
                        <div className={css.cross}>
                            <img
                                src={require("../../images/modal/grey.svg")}
                                style={{ width: `20px` }}
                                onClick={() => closeSlide()}
                                alt="menu drawer"
                            />
                        </div>
                        {windowLocation.pathname === "/" && (
                            <div
                                className={css.helpDraw}
                                onClick={() => HelpDropDown()}
                            >
                                {" "}
                                <span>HELP & SUPPORT</span>{" "}
                            </div>
                        )}
                        {(windowLocationHref.includes("eventList") ||
                            windowLocationHref.includes("eventDetails")) && (
                            <Link
                                style={{ color: "#4a4a4a" }}
                                to="/"
                                className={css.helpDraw}
                                onClick={() => HelpDropDown()}
                            >
                                {" "}
                                <span>HELP & SUPPORT</span>{" "}
                            </Link>
                        )}
                        {windowLocation.pathname === "/" && (
                            <div
                                className={css.platformDraw}
                                onClick={() =>
                                    platformDropDown(windowLocation.pathname)
                                }
                            >
                                <span>PLATFORMS </span>{" "}
                            </div>
                        )}
                        {(windowLocationHref.includes("eventList") ||
                            windowLocationHref.includes("eventDetails")) && (
                            <Link
                                style={{ color: "#4a4a4a" }}
                                to="/"
                                className={css.platformDraw}
                                onClick={() =>
                                    platformDropDown(windowLocation.pathname)
                                }
                            >
                                <span>PLATFORMS </span>{" "}
                            </Link>
                        )}
                        <Link
                            to="/eventList"
                            className={css.eventsDraw}
                            // onClick={() => teamScrollDown()}
                        >
                            EVENTS
                        </Link>
                        {windowLocation.pathname === "/" && (
                            <div
                                className={css.aboutUs}
                                onClick={() => teamScrollDown()}
                            >
                                ABOUT US
                            </div>
                        )}

                        {(windowLocationHref.includes("eventList") ||
                            windowLocationHref.includes("eventDetails")) && (
                            <Link
                                style={{ color: "#4a4a4a" }}
                                to="/"
                                className={css.aboutUs}
                                onClick={() => teamScrollDown()}
                            >
                                ABOUT US
                            </Link>
                        )}
                    </div>
                </div>
                <Link to="/">
                    <div className={css.imgcontainer}>
                        <img
                            ref={ref => {
                                headerLogo = ref;
                            }}
                            className={css.headerLogo}
                            style={{
                                marginBottom: `0`,
                                marginLeft: `2em`,
                                width: `5em`,
                            }}
                            src={icon}
                            alt="ccoe"
                        />
                    </div>
                </Link>
                <div className={css.headerRightSection}>
                    <div
                        className={css.nav}
                        ref={ref => (navNodeRef = ref)}
                        onMouseLeave={() => hideNavBar()}
                    >
                        <div
                            data-target={"support"}
                            onMouseEnter={e => onHeaderIconHover(e)}
                            className={css.navItem}
                        >
                            {windowLocation.pathname === "/" && (
                                <span
                                    onClick={() =>
                                        HelpDropDown(windowLocation.pathname)
                                    }
                                >
                                    HELP & SUPPORT{" "}
                                </span>
                            )}
                            {(windowLocationHref.includes("eventList") ||
                                windowLocationHref.includes(
                                    "eventDetails"
                                )) && (
                                <Link
                                    className={
                                        css.linkStyle + " " + css.platForm
                                    }
                                    to="/"
                                    onClick={() => goToHelpSection()}
                                >
                                    HELP & SUPPORT
                                </Link>
                            )}
                            <i className={css.arrow} />
                        </div>
                        <div
                            data-target={"platform"}
                            onMouseEnter={e => onHeaderIconHover(e)}
                            className={css.navItem}
                        >
                            {windowLocation.pathname === "/" && (
                                <span
                                    onClick={() =>
                                        platformDropDown(
                                            windowLocation.pathname
                                        )
                                    }
                                >
                                    {" "}
                                    PLATFORMS
                                </span>
                            )}
                            {(windowLocationHref.includes("eventList") ||
                                windowLocationHref.includes(
                                    "eventDetails"
                                )) && (
                                <Link
                                    className={
                                        css.linkStyle + " " + css.platForm
                                    }
                                    to="/"
                                    onClick={() => goToPlatformSection()}
                                >
                                    PLATFORMS
                                </Link>
                            )}
                            <i className={css.arrow} />
                        </div>
                    </div>
                    {windowLocation.pathname === "/" && (
                        <div
                            className={css.navItem}
                            onClick={() =>
                                teamScrollDown(windowLocation.pathname)
                            }
                        >
                            ABOUT US{" "}
                        </div>
                    )}
                    {(windowLocationHref.includes("/eventList") ||
                        windowLocationHref.includes("eventDetails")) && (
                        <Link
                            className={css.linkStyle + " " + css.navItem}
                            to="/"
                            onClick={() => goToAboutUsSection()}
                        >
                            ABOUT US
                        </Link>
                    )}
                    <Link
                        className={css.navItem + " " + css.linkStyle}
                        to="/eventList"
                    >
                        EVENTS
                    </Link>
                    {/* <div className={css.navItem} onClick={() => showFeedback()}>
                        <div className={css.feedBackWrapper}>FEEDBACK</div>
                    </div> */}
                </div>
            </div>
            <nav
                ref={ref => (navBarRef = ref)}
                onMouseLeave={() => hideNavBar()}
                onMouseEnter={() => showNavbar()}
                className={css.navBar}
            >
                {renderNav()}
            </nav>
            {/* {isFeedBack ? (
                <FeedBack
                    closeModal={value => {
                        setFeedback(value);
                    }}
                />
            ) : null} */}
        </header>
    );
};
export default Header;
