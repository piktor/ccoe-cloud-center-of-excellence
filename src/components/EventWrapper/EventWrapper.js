import React from "react";
import { Link } from "gatsby";
import moment from "moment";
import css from "./Event.module.css";

const EventWrapper = props => {
    var slugPath = props.data.node.fields.slug;
    var data = props.data.node.frontmatter;

    const getDate = (startDate, endDate) => {
        const startDay = moment(startDate).format("Do");
        const endDay = moment(endDate).format("Do");
        const startMonth = moment(startDate).format("MMM");
        const endMonth = moment(endDate).format("MMM");
        const startTime = moment(startDate).format("LT");
        const endTime = moment(endDate).format("LT");

        if (startDay === endDay && startMonth === endMonth) {
            return `${startDay} ${startMonth}, ${startTime} -${endTime}`;
        } else if (startDay !== endDay && startMonth === endMonth) {
            return `${startDay}-${endDay} ${startMonth}, ${startTime}-${endTime}`;
        } else
            return `${startDay} ${startMonth}-${endDay} ${" " +
                endMonth}, ${startTime} - ${endTime}`;
    };
    return (
        <Link to={slugPath}>
            <div className={css.wrapperContainer}>
                <div className={css.wrapperleftSection}>
                    <div className={css.calenderIcon}>
                        {" "}
                        <img
                            src={require("../../images/icon-calendar.png")}
                            alt="calendar"
                        />
                    </div>
                </div>
                <div className={css.wrapperMiddleSection}>
                    <div className={css.eventDetails}>
                        <div className={css.eventName}>{data.name}</div>
                        <div className={css.eventDesc}>{data.description} </div>
                    </div>
                    <div className={css.location}>
                        <img
                            className={css.timeIcon}
                            src={require("../../images/icon_time.svg")}
                            alt=""
                        />

                        <div style={{ opacity: "0.7" }}>
                            {getDate(data.startDate, data.endDate)}
                        </div>
                    </div>
                    <div className={css.wrapperRightSection}>
                        <div className={css.location}>
                            {" "}
                            <img
                                className={css.timeIcon}
                                src={require("../../images/icon_map_pointer.svg")}
                                alt=""
                            />
                            <div style={{ opacity: "0.7" }}>
                                {" "}
                                {data.location
                                    ? data.location
                                    : "No address...!"}
                            </div>
                        </div>
                    </div>
                </div>
                <div className={css.arrowContainer}>
                    <i className={css.arrow} />
                </div>
            </div>
        </Link>
    );
};

export default EventWrapper;
