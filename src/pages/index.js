import React from "react";
import Section from "../components/Section/Section";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { graphql } from "gatsby";

const IndexPage = ({ data }) => (
    <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Section
            blogs={data.blogs}
            support={data.support}
            platforms={data.platforms}
            events={data.events}
        />
    </Layout>
);

export const pagelistQuery = graphql`
    query BlogIndexQuery {
        blogs: allMarkdownRemark(
            filter: { frontmatter: { draft: { eq: false } } }
        ) {
            edges {
                node {
                    id
                    fileAbsolutePath
                    frontmatter {
                        title
                        author
                        date
                    }
                    excerpt
                }
            }
        }
        support: allMarkdownRemark(
            filter: { frontmatter: { support: { eq: true } } }
        ) {
            edges {
                node {
                    frontmatter {
                        title
                        description
                        order
                        image
                        link
                    }
                }
            }
        }
        platforms: allMarkdownRemark(
            filter: { frontmatter: { platform: { eq: true } } }
        ) {
            edges {
                node {
                    frontmatter {
                        title
                        description
                        order
                        image
                        link
                    }
                }
            }
        }
        events: allMarkdownRemark(
            filter: {
                frontmatter: {
                    event: { eq: true }
                    featuredEvent: { eq: false }
                }
            }
        ) {
            edges {
                node {
                    fields {
                        slug
                    }
                    frontmatter {
                        name
                        description
                        coverImage
                        startDate
                        endDate
                        location
                    }
                    excerpt
                }
            }
        }
    }
`;

export default IndexPage;
