import React from "react";
import { graphql } from "gatsby";

const BlogList = () => {
    return <div>some thing here.</div>;
};

export const pagelistQuery = graphql`
    query BlogList {
        allMarkdownRemark {
            edges {
                node {
                    id
                }
            }
        }
    }
`;
export default BlogList;
