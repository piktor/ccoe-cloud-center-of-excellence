+++
name= "Hackathon"
descriptionTitle= "Hackathon"
description= "This year, we will challenge student teams to innovate on how to apply xR technology to the Metro by T-Mobile brand in Retail, Care, and beyond. We are looking for volunteers in the area to mentor, judge, and assist with other duties. Reach out to P&T Tech Events for more information."

#startDate and endDate shoul be in below format TO ADD to calender details
#time will be in PST(UTC - 7:00) format
startDate= "2019-10-26 09:00:00"
endDate= "2019-10-27 17:00:00"
location= "Dallas, TX | University of Texas"
featuredEvent= true
event= true

+++

#Details abut event 1
is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.