+++
title= "Next"
description= "An open-source Identity and Access Management Platform. It leverages the Sawtooth blockchain improve the status-quo of identity governance."
image= "logo-next-red.svg"
link= "https://github.com/tmobile/sawtooth-next-directory "
order= 2
platform= true
+++