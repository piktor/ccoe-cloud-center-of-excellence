+++
title= "Telemetry"
description= "Prometheus/Grafana-based metrics collection, visualization and alerting at scale. Supports cloud and on-prem apps."
image= "logo-ims-red.svg"
link= "https://prd.web.css.kube.t-mobile.com/telemetry"
order= 4
platform= true
+++