+++
title= "Jazz Serverless"
description= "Jazz lets developers quickly create serverless applications with a click of a button. Its modular design makes it easy to add new integrations."
image= "logo-jazz-red.svg"
link= "https://jazz.corporate.t-mobile.com/ "
order= 1
platform= true
+++