import React, { useState, useEffect } from "react";
import { graphql } from "gatsby";
import css from "./eventList/EventList.module.css";
import EventWrapper from "../components/EventWrapper/EventWrapper";
import Event from "../components/Section/Slider/Event/Event";
import Layout from "../components/layout";

const EventList = ({ data }) => {
    const searchRef = React.createRef();
    const [upComingEventsData, setUpComingEventsData] = useState(
        data.upComingEvents.edges
    );
    const [featuredEventsData, setFeaturedEventsData] = useState(
        data.featuredEvent.edges
    );
    const [noUpcomingEvents, setnoUpcomingEvents] = useState(false);
    const [nofeaturedEvents, setnofeaturedEvents] = useState(false);

    function searchInputChanged(event) {
        let filteredData = data.eventsList.edges.filter(
            el =>
                el.node.frontmatter.name
                    .toLowerCase()
                    .indexOf(event.target.value.toLowerCase()) > -1
        );
        let filteredFeaturedData = filteredData.filter(
            el => el.node.frontmatter.featuredEvent === true
        );
        setFeaturedEventsData(filteredFeaturedData);
        if (filteredFeaturedData.length === 0) {
            setnofeaturedEvents(true);
        } else {
            setnofeaturedEvents(false);
        }
        let filteredUpcomingData = filteredData.filter(
            el => el.node.frontmatter.featuredEvent === false
        );
        setUpComingEventsData(filteredUpcomingData);
        if (filteredUpcomingData.length === 0) {
            setnoUpcomingEvents(true);
        } else {
            setnoUpcomingEvents(false);
        }
    }

    const showSearchBar = e => {
        let searchBarNode = document.getElementById("searchInput");
        if (searchRef.current.contains(e.target)) {
            searchBarNode.classList.add(css.searchCustomStyle);
            document.getElementById("inputReset").style.display = "block";
        } else {
            searchBarNode.classList.remove(css.searchCustomStyle);
            document.getElementById("inputReset").style.display = "none";
            setFeaturedEventsData(data.featuredEvent.edges);
            setUpComingEventsData(data.upComingEvents.edges);
            setnoUpcomingEvents(false);
            setnofeaturedEvents(false);
        }
    };
    const handleBlur = e => {
        let searchBarNode = document.getElementById("myInput");

        if (searchBarNode.value === "") {
            searchBarNode.value = "";
        } else searchBarNode.value = "";
    };
    useEffect(() => {
        document.addEventListener("click", showSearchBar);

        return () => {
            document.removeEventListener("click", showSearchBar);
        };
    });
    return (
        <Layout eventListingPage={true}>
            <div className={css.eventListContainer}>
                <div className={css.featuredEventSection}>
                    <div className={css.headingWrapper}>
                        <div className={css.heading}>Featured Events</div>
                        <div style={{ position: "relative" }}>
                            <form
                                style={{ margin: "0" }}
                                action=""
                                method="get"
                            >
                                <div
                                    id="searchInput"
                                    className={css.searchbox}
                                    ref={searchRef}
                                    onClick={e => showSearchBar(e)}
                                    style={{ display: "flex", width: "" }}
                                >
                                    {" "}
                                    <img
                                        className={css.searchIcon}
                                        src={require("../images/magnifying-glass.svg")}
                                        style={{
                                            width: "1em",
                                            height: "1em",
                                            opacity: "0.7",
                                        }}
                                        alt="Search"
                                    />
                                    <input
                                        type="search"
                                        className={css.input}
                                        id="myInput"
                                        placeholder="Search"
                                        autoComplete="off"
                                        onChange={searchInputChanged}
                                        onBlur={handleBlur}
                                    />
                                </div>
                                <input
                                    type="reset"
                                    id="inputReset"
                                    alt="clear"
                                    value=""
                                    className={css.inputClose}
                                />
                            </form>
                        </div>
                    </div>
                    <div
                        className={
                            nofeaturedEvents
                                ? css.featuredEventsTilesNoResults
                                : css.featuredEventsTiles
                        }
                    >
                        {renderFeaturedEvents(
                            featuredEventsData,
                            nofeaturedEvents
                        )}
                    </div>
                </div>
                <div className={css.upcomingEventSection}>
                    <div className={css.heading}>All Upcoming Events</div>
                    <hr />
                    {upComingEventsData.length > 0 ? (
                        <div className={css.eventWrapperHeading}>
                            <div className={css.eventDetails}>
                                Event Details
                            </div>
                            <div className={css.eventDateAndTime}>
                                Date & Time
                            </div>
                            <div className={css.eventLocation}>Location</div>
                        </div>
                    ) : null}
                    {renderUpcomingEvents(upComingEventsData, noUpcomingEvents)}
                </div>
            </div>
        </Layout>
    );
};
const renderUpcomingEvents = (upComingEventsData, noUpcomingEvents) => {
    if (noUpcomingEvents) {
        return (
            <div className={css.noResults}>
                <img src={require("../images/no-events.svg")} alt="no events" />
                <div>No Events Found..!!</div>
                <div
                    style={{
                        opacity: "0.5",
                        fontSize: "0.9em",
                        paddingBottom: "0.6em",
                    }}
                >
                    There is no events planned yet
                </div>
            </div>
        );
    } else {
        return upComingEventsData.map((item, key) => {
            return <EventWrapper data={item} key={key} />;
        });
    }
};
const renderFeaturedEvents = (featuredEventsData, nofeaturedEvents) => {
    if (nofeaturedEvents) {
        return (
            <div className={css.noResults}>
                <img src={require("../images/no-events.svg")} alt="no events" />
                <div>No Events Found..!!</div>
                <div style={{ opacity: "0.5", fontSize: "0.9em" }}>
                    There is no events planned yet
                </div>
            </div>
        );
    } else {
        return featuredEventsData.map((item, key) => {
            return <Event data={item} key={key} />;
        });
    }
};

export const pagelistQuery = graphql`
    query BlogIndexQuery2 {
        eventsList: allMarkdownRemark(
            filter: { frontmatter: { event: { eq: true } } }
        ) {
            edges {
                node {
                    id
                    fields {
                        slug
                    }
                    frontmatter {
                        path
                        name
                        descriptionTitle
                        description
                        coverImage
                        startDate
                        endDate
                        location
                        featuredEvent
                        order
                    }
                    excerpt
                }
            }
        }
        featuredEvent: allMarkdownRemark(
            filter: {
                frontmatter: {
                    event: { eq: true }
                    featuredEvent: { eq: true }
                }
            }
        ) {
            edges {
                node {
                    id
                    fields {
                        slug
                    }
                    frontmatter {
                        path
                        name
                        descriptionTitle
                        description
                        coverImage
                        startDate
                        endDate
                        location
                        featuredEvent
                    }
                    excerpt
                }
            }
        }
        upComingEvents: allMarkdownRemark(
            filter: {
                frontmatter: {
                    event: { eq: true }
                    featuredEvent: { eq: false }
                }
            }
        ) {
            edges {
                node {
                    id
                    fields {
                        slug
                    }
                    frontmatter {
                        path
                        name
                        descriptionTitle
                        description
                        coverImage
                        startDate
                        endDate
                        location
                        featuredEvent
                    }
                    excerpt
                }
            }
        }
    }
`;
export default EventList;
