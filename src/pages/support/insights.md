+++
title= "Insights"
description= "Detailed cost reporting for cloud workloads."
image= "logo-insight-red.svg"
link= "https://app.powerbi.com/groups/me/apps/a5548ee4-ce6b-4646-937d-448d333a7458/reports/c6bd15ed-ab29-480b-9f99-6c26d5d4c593/ReportSection0db6ceea01a693c5e87f?chromeless=1"
order= 6
support= true
+++