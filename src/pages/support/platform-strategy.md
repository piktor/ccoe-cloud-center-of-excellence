+++
title= "Platform Strategy"
description= "Find resources to help understand and strategize the platform you want to choose."
image= "icon-platform-strategy.svg"
order= 1
link= "https://d2pc1vq36vbjlv.cloudfront.net/"
support= true
+++
