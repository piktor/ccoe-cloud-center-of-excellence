+++
title= "Intake"
description= "Submit new requests to track and monitor issues across your cloud applications, via a single request form."
image= "icon-intake.svg"
link= "https://tmobileusa.sharepoint.com/sites/CloudIntake "
order= 2
support= true
+++