/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 *
 */
const path = require("path");
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.onCreateWebpackConfig = ({ config, stage }) => {
    if (stage === "build-javascript") {
        if (config !== undefined) {
            config.merge({
                entry: { app: ["babel-polyfill", config.resolve().entry.app] },
            });
        }
    }

    return config;
};

exports.onCreateNode = ({ node, getNode, actions }) => {
    const { createNodeField } = actions;
    if (node.internal.type === `MarkdownRemark`) {
        const slug = createFilePath({ node, getNode, basePath: `pages` });
        createNodeField({
            node,
            name: `slug`,
            value: slug,
        });
    }
};

exports.createPages = ({ graphql, actions }) => {
    // **Note:** The graphql function call returns a Promise
    const { createPage } = actions;
    return new Promise((resolve, reject) => {
        const eventDetailsTemplate = path.resolve(
            `./src/templates/eventDetails.js`
        );
        resolve(
            graphql(`
                {
                    allMarkdownRemark(
                        filter: { frontmatter: { event: { eq: true } } }
                    ) {
                        edges {
                            node {
                                id
                                fields {
                                    slug
                                }
                                frontmatter {
                                    path
                                    name
                                    description
                                    coverImage
                                    startDate
                                    location
                                    featuredEvent
                                }
                            }
                        }
                    }
                }
            `).then(result => {
                if (result.errors) {
                    reject(result.errors);
                }
                // Create pages for each markdown file.
                result.data.allMarkdownRemark.edges.forEach(({ node }) => {
                    createPage({
                        path: node.fields.slug,
                        component: eventDetailsTemplate,
                        context: {},

                        // In your eventDetailsTemplate template's graphql query, you can use path
                        // as a GraphQL variable to query for data from the markdown file.
                    });
                    resolve();
                });
            })
        );
    });
};
